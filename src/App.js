import React, { Component } from 'react';
import './App.css';

function pay(paymentType) {
  window.PaymentSession.updateSessionFromForm('card');
}

window.PaymentSession.callback(value => console.log({value}))


class App extends Component {
  componentDidMount() {
    try {
      const sesionScript = document.createElement('script');
      sesionScript.src =
        'https://ap-gateway.mastercard.com/form/version/46/merchant/61056001/session.js';
      sesionScript.async = true;
      setTimeout(function() {
        document.body.appendChild(sesionScript);
      }, 2000);
      try {
        const sesionScript = document.createElement('script');
        sesionScript.src = './payment.js';
        setTimeout(function() {
          document.body.appendChild(sesionScript);
        }, 5000);
      } catch (error) {
        console.log({ error });
      }
    } catch (error) {
      console.log({ error });
    }
  }

  pay = () => {
    console.log('payment called');
    window.pay();
    // window.PaymentSession.updateSessionFromForm("card")
  };

  render() {
    console.log({ window });
    const inputId = document.getElementById('card-number');
    // window.PaymentSession.callback(value => console.log({value}))
    // console.log({inputId})
    // window.PaymentSession.configure({
    //   fields: {
    //     // Attach hosted fields to your payment page
    //     card: {
    //       number: '#card-number',
    //       securityCode: '#security-code',
    //       expiryMonth: '#expiry-month',
    //       expiryYear: '#expiry-year'
    //     }
    //   },
    //   frameEmbeddingMitigation: ['javascript'],
    //   callbacks: {
    //     initialized: function(response) {
    //       // HANDLE INITIALIZATION RESPONSE
    //       console.log({ response });
    //       if (response.status === 'ok') {
    //         document.getElementById('visaCheckoutButton').style.display =
    //           'block';
    //       }
    //     },

    //     formSessionUpdate: function(response) {
    //       // HANDLE RESPONSE FOR UPDATE SESSION
    //       if (response.status) {
    //         if ('ok' == response.status) {
    //           console.log({ response });
    //           console.log('Session updated with data: ' + response.session.id);

    //           //check if the security code was provided by the user
    //           if (response.sourceOfFunds.provided.card.securityCode) {
    //             console.log('Security code was provided.');
    //           }

    //           //check if the user entered a MasterCard credit card
    //           if (response.sourceOfFunds.provided.card.scheme == 'MASTERCARD') {
    //             console.log('The user entered a MasterCard credit card.');
    //           }
    //         } else if ('fields_in_error' == response.status) {
    //           console.log('Session update failed with field errors.');
    //           if (response.errors.cardNumber) {
    //             console.log('Card number invalid or missing.');
    //           }
    //           if (response.errors.expiryYear) {
    //             console.log('Expiry year invalid or missing.');
    //           }
    //           if (response.errors.expiryMonth) {
    //             console.log('Expiry month invalid or missing.');
    //           }
    //           if (response.errors.securityCode) {
    //             console.log('Security code invalid.');
    //           }
    //           if (response.errors.number) {
    //             console.log('Gift card number invalid or missing.');
    //           }
    //           if (response.errors.pin) {
    //             console.log('Pin invalid or missing.');
    //           }
    //           if (response.errors.bankAccountHolder) {
    //             console.log('Bank account holder invalid.');
    //           }
    //           if (response.errors.bankAccountNumber) {
    //             console.log('Bank account number invalid.');
    //           }
    //           if (response.errors.routingNumber) {
    //             console.log('Routing number invalid.');
    //           }
    //         } else if ('request_timeout' == response.status) {
    //           console.log(
    //             'Session update failed with request timeout: ' +
    //               response.errors.message
    //           );
    //         } else if ('system_error' == response.status) {
    //           console.log(
    //             'Session update failed with system error: ' +
    //               response.errors.message
    //           );
    //         }
    //       } else {
    //         console.log('Session update failed: ' + response);
    //       }
    //     }
    //   },
    //   order: {
    //     amount: 10.0,
    //     currency: 'AUD'
    //   }
    // });
    return (
      <div className="App" onLoad={() => console.log('loaded')}>
        <div ref={el => (this.instance = el)} />
        <h3>Credit Card</h3>
        <div>
          Card Number:{' '}
          <input
            type="text"
            id="card-number"
            className="input-field"
            value=""
            readOnly
          />
        </div>
        <div>
          Expiry Month:<input
            type="text"
            id="expiry-month"
            className="input-field"
          />
        </div>
        <div>
          Expiry Year:<input
            type="text"
            id="expiry-year"
            className="input-field"
          />
        </div>
        <div>
          Security Code:<input
            type="text"
            id="security-code"
            className="input-field"
            value=""
            readOnly
          />
        </div>

        <button id="visaCheckoutButton" onClick={this.pay}>
          Pay Now
        </button>
      </div>
    );
  }
}

export default App;